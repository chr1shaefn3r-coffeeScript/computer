#!/usr/bin/env bash
# Installs signal-cli into /opt

if [ -z $HUBOT_SIGNAL_USERNAME ]; then
	echo "Please provide HUBOT_SIGNAL_USERNAME as environment variable!";
	echo "Needs to be registered with:"
	echo "$ signal-cli -u NUMBER register";
	echo "$ signal-cli -u NUMBER verify CODE";
	exit 1;
fi

apt-get update && apt-get install --yes libunixsocket-java openjdk-8-jdk

adduser --disable-password --disable-login signal-cli

SIGNAL_CLI_VERSION=0.5.2
wget -O- https://github.com/AsamK/signal-cli/releases/download/v"$SIGNAL_CLI_VERSION"/signal-cli-"$SIGNAL_CLI_VERSION".tar.gz | tar -xzC /opt
ln -sf /opt/signal-cli-"$SIGNAL_CLI_VERSION"/bin/signal-cli /usr/local/bin/
mkdir /opt/src/
wget -O- https://github.com/AsamK/signal-cli/archive/v"$SIGNAL_CLI_VERSION".tar.gz | tar -xzC /opt/src
mv /opt/src/signal-cli-"$SIGNAL_CLI_VERSION"/* /opt/signal-cli-"$SIGNAL_CLI_VERSION"
cp /opt/signal-cli-"$SIGNAL_CLI_VERSION"/data/org.asamk.Signal.conf /etc/dbus-1/system.d/
cp /opt/signal-cli-"$SIGNAL_CLI_VERSION"/data/org.asamk.Signal.service /usr/share/dbus-1/system-services/
cp /opt/signal-cli-"$SIGNAL_CLI_VERSION"/data/signal.service /etc/systemd/system/
sed -i -e "s|%dir%|/usr/local/|" -e "s|%number%|$HUBOT_SIGNAL_USERNAME|" /etc/systemd/system/signal.service
systemctl daemon-reload
systemctl enable signal.service
systemctl reload dbus.service

