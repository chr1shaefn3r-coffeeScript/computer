set -e;
echo "HUBOT_SIGNAL_USERNAME=$HUBOT_SIGNAL_USERNAME" >> ./env/common.env

echo "sed -i -e \"s|%IMAGEURL%|$DOCKER_REGISTRY/$JOB_NAME:latest|\" docker-compose.yml"
sed -i -e "s|%IMAGEURL%|$DOCKER_REGISTRY/$JOB_NAME:latest|" docker-compose.yml

echo "cat docker-compose.yml"
cat docker-compose.yml

echo "sudo docker-compose -p "$JOB_NAME-dev" -f docker-compose.yml stop";
sudo docker-compose -p "$JOB_NAME-dev" -f docker-compose.yml stop

# -v => Remove volumes associated with containers
echo "sudo docker-compose -p "$JOB_NAME-dev" -f docker-compose.yml rm --force -v";
sudo docker-compose -p "$JOB_NAME-dev" -f docker-compose.yml rm --force -v

echo "sudo docker-compose -p "$JOB_NAME-dev" -f docker-compose.yml up -d --force-recreate";
sudo docker-compose -p "$JOB_NAME-dev" -f docker-compose.yml up -d --force-recreate

